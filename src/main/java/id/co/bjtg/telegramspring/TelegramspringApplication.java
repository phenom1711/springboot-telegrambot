package id.co.bjtg.telegramspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class TelegramspringApplication {

	public static void main(String[] args) {

		ApiContextInitializer.init();
		SpringApplication.run(TelegramspringApplication.class, args);
	}
}
